# HW0 Xiaowei Zhang 2022/04/01

1. I'm excited about getting to learn about big data and how to work with data wisely. Generally interested in everything listed in syllabus!

2. Previously I have no experience with linux system and big data computation. I guess that I might spend more time getting myself familiar with everything.

3. I want to learn how to archive and store data and codes with clarity.

4. I hope that I can take advantage of big data computation in my research to guide my design of new molecular biology tools.

5. I'd like to work with protein sequence libraries and structural predictions. This could help me engineer my enzymes.



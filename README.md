# HW0

Non-graded practice HW

## Goals

The goal of this homework assignment is to get a feel for some of the basic tools used in this course to process and manage data and code.

This assignment will involve:
- using [`git`](https://git-scm.com/docs/git): [clone](https://git-scm.com/docs/git-clone), [add](https://git-scm.com/docs/git-add), [commit](https://git-scm.com/docs/git-commit), [push](https://git-scm.com/docs/git-push)
- creating and editing files using a command line text editor
- transferring files between your local computer and FarmShare using [`rsync`](https://linux.die.net/man/1/rsync)/[`scp`](https://linux.die.net/man/1/scp)/[pscp](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
- transferring files between FarmShare and cloud spaces using [`rclone`](https://rclone.org)
- using [`zip`](https://linux.die.net/man/1/zip) to create zip files

## Instructions

1. Use `git` to clone this repository to a space on your FarmShare [home directory](https://en.wikipedia.org/wiki/Home_directory)
    - short [git tutorial](https://www.tutorialspoint.com/git/git_basic_concepts.htm)
    - encourage the use of ssh keys, review the "FarmShare & Gitlab Basics" Canvas Page
2. Create a blank [markdown file](https://www.markdownguide.org/getting-started) called `HW0.md` in this repository using a text editor.
    - I recommend learning [vim](https://www.linux.com/training-tutorials/vim-101-beginners-guide-vim)
    - [nano](https://www.howtogeek.com/howto/42980/the-beginners-guide-to-nano-the-linux-command-line-text-editor) is also available, and while it is easier to start with, it is far less capable
3. In this new file, answer the following questions using [markdown syntax](https://www.markdownguide.org/basic-syntax) (use no more than two/three sentences for each question):
    1. What excites you about this course?
    2. What worries you about this course?
    3. What one skill are you most hoping to learn and why? 
    4. What role do you think data management will play in your career?
    5. What type of dataset are you hoping to work with this quarter and why?
4. Add the file to the repository and commit the changes to your local repository
5. Push a copy of the local repository to a private repository in your Stanford GitLab space
5. Print a PDF version of the rendered markdown from the view of your repository on Stanford GitLab
6. Copy the PDF into your working directory on FarmShare and add/commit/push it
    - use `rsync`/`scp` (Mac) or [pscp](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html) (Windows)
7. Make this PDF accessible via the web by placing it in your `WWW` directory within your [AFS space](https://uit.stanford.edu/service/afs/intro)
    - Your AFS home directory can be accessed via FarmShare. It is the `afs-home` directory within your FarmShare [home directory](https://en.wikipedia.org/wiki/Home_directory).
8. Copy the PDF to your Stanford Google Drive using rclone
    - This first setting up an rclone remote against your Stanford Google Drive Account. Instructions on how to do this are in the `Large Data Transfers with rclone` video within `Video Tutorials` section of [datausers.stanford.edu](http://datausers.stanford.edu). You can skip the section from 1:30 - 4:10.
    - You do not need to make your own `client_id`/`client_secret`. Use the following for the `client_id` and `client_secret` set up for the course:
        - `client_id` : `496632253402-oukauk06a4mas3lje3hgs24eur832gi0.apps.googleusercontent.com`
        - `client_secret` : `WqA-m1Mj_z5kfMnwbtj3QuDO`
9. Copy the PDF to your class Google Cloud Storage space using rclone
    - This requires a new rclone remote that is set against the BioE 301P Google Cloud Project, named `soe-bioe-301p`. The Cloud Storage bucket you will upload to is `22s-bioe-301p-<SUNetID>`
10. create a zip file of your repository on FarmShare, excluding .git directory
    - `zip -r <zip file to create> <path to directory to zip> -x '*.git*'`
11. Copy this zip file to your local computer
12. Submit this zip file for HW0 to eduflow.com
